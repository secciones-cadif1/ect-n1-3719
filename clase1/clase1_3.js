
const ProgressBar = require("progress")
const moment = require('moment');

var now = moment();
console.log(now);

// se crear una progressBar con 15 pasos
let bar = new ProgressBar(":bar",{total:15})
// cada 250 ms se avanza un paso de la barra
let timer = setInterval(()=>{
    bar.tick();
    if (bar.complete){
        clearInterval(timer)
        leerDatos();
    }
},150)

function leerDatos(){
    // usar el modulo readline de Node
    let teclado = require("readline").createInterface({
        input:process.stdin, // la entrada standard, es decir, el teclado
        output:process.stdout // la salida standar, es decir, la pantalla
    })

    teclado.question("Introduzca el maximo:", function(dato){
        console.log(`El dato leido es ${dato}`)
        calcular(dato)
        process.exit() // termina la ejecucion de la aplicacion
    })
}

function calcular(x){    
    console.log(`Vamos a calcular usando ${x}`)
}