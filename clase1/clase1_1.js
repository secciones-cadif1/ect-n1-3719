
console.log("Primera clase de Electron JS")

function calcularPromedio(acum,cont){
    if (cont!=0)
        return acum/cont;
    else
        return 0;
}
let persona ={
    "nombre":"Jose Luis",
    "edad":42,
    "sueldo":500
}
let diasSemana = ["Lunes","Martes","Miercoles","Jueves","Viernes"];
let cont=0;
let acum=0;
let edad;
do{
    edad = Math.round(Math.random()*100);
    //console.log(`La edad es ${edad}`)
    cont++;
    acum+=edad;
}while (edad != 50);
let promedio = calcularPromedio(acum,cont);
console.log(`Se generaron ${cont} edades antes de conseguir el 50`)
console.log(`El promedio de edades es ${promedio.toFixed(2)}`)
console.log(`El dia de la semana es ${diasSemana[3].toUpperCase()}`)
console.log(`El trabajador ${persona.nombre} tiene ${persona.edad} años`)