
const { error } = require("console");
const {app,dialog, BrowserWindow} = require("electron");
const { argv } = require("process");

let preguntar =true;

console.log(argv);
dialog.showErrorBox("Error","No se encontro el archivo xyz.xls")

app.on("ready",()=>{
     // la ventana se crea pero no es visible
     let main = new BrowserWindow({
        show:false,
        resizable:false,
    });
    // showMessageBoxSync bloquea la ejecucion del proceso
    // en cambio showMessageBox no bloquea el proceso
    dialog.showMessageBox(main,{
        message:"Debe tener suficiente espacio en disco",
        title:"Importante",
        type:"info"
    })
    console.log("luego del info")
   
    main.loadURL("https://cadif1.com")

    let presentacion=new BrowserWindow({
        center:true,
        frame:false,
        width:500,
        height:320,
        show:false
    });
    presentacion.loadFile("presentacion.html",)
    presentacion.on("ready-to-show",()=>{
        presentacion.show();
    })
    presentacion.on("show",()=>{
        console.log("on show presentacion");
        
        console.log("set timout")
        // cuando este lista para mostrarla
        main.on("ready-to-show",()=>{
            presentacion.close();
            // se muestra le ventana
            console.log("esta lista la ventana main...")
            main.show()
        })
        main.on("close",(event)=>{
            console.log("se va a cerrar la ventana main")
            // siempre debo cancelar el cierre cuando uso
            // showMessageBox
            if (preguntar){
                event.preventDefault();
                dialog.showMessageBox(main,{
                    "title":"Confirmacion",
                    "message":"Esta seguro que desea salir ?",
                    "type":"question",
                    "noLink":true,
                    "checkboxLabel": "No volver a preguntar",
                    "buttons":[
                        "Si","No"
                    ]
                }).then((resp)=>{
                    if (resp.response==0)
                        app.exit();
                    else
                        if (resp.checkboxChecked)
                            preguntar=false;
                })
            }
            /*let resp=dialog.showMessageBoxSync(main,{
                "title":"Confirmacion",
                "message":"Esta seguro que desea salir ?",
                "type":"question",
                "noLink":true,
                "buttons":[
                    "Si","No"
                ]
            });
            // se el usuario hace click sobre el boton SI
            if (resp==0)
                app.exit(0);
            else
                event.preventDefault()*/
        })
        
    })
})