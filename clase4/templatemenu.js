const {BrowserWindow,app,nativeImage} = require("electron")

module.exports.templateMainMenu = [
    {
        label:"Archivo",
        submenu:[
            {
                label:"Nuevo archivo para programar",
                click:()=>{
                    console.log("archivo")
                    let win2 = new BrowserWindow();
                }
            },
            {
                label:"Abrir",
                enabled: false,
                icon: nativeImage.createFromPath('logo.png'),
                click:()=>{

                }
            },
            {
                type: "separator"
            },
            {
                label:"Salir",
                click: ()=>{
                    app.quit();
                }
            }
        ]        
    },
    {
        label:"Editar",
        submenu:[
            {
                label:"Esconder menu automaticamente",
                type:"checkbox",
                checked:true,
                click:(menuItem, browserWindow, event)=>{
                    console.log(menuItem.checked)
                    browserWindow.autoHideMenuBar = menuItem.checked
                    // si no se debe esconder automaticamente, entonces la barra de menu
                    // se muestra
                    if (!menuItem.checked)
                        browserWindow.menuBarVisible  = true
                }
            },
            {
                label:"Tipo",
                submenu :[
                    {
                        label: "A",
                        type:"radio",
                    },
                    {
                        label: "B",
                        type:"radio",
                    },
                    {
                        label: "C",
                        type:"radio",
                    }
                ]
            },
            {
                role: "minimize"
            },
            {
                role:"toggleDevTools"
            },
            {
                role:"reload"
            }

        ]
        
    },
    {
        label:"Vista",
        click:()=>{
            console.log("Vista")
        }
    },
    {
        label:"Perfil",
        click:()=>{
            console.log("Perfil")
        }
    }
]