const {Menu,BrowserWindow,app}=require("electron")
// se carga la plantilla desde el archivo
let {templateMainMenu} = require("./templatemenu.js")
console.log(templateMainMenu)
// construyo el menu en base a la plantilla
let mainMenu = Menu.buildFromTemplate(templateMainMenu);

app.on("ready",()=>{
    let menu = Menu.getApplicationMenu();
    Menu.setApplicationMenu(null);
    let win=new BrowserWindow({
        title:"principal",
        show:false,
        autoHideMenuBar:true
    })
    win.loadURL("http://cadif1.com")
    win.setMenu(mainMenu)
    win.on("ready-to-show",()=>{
        console.log("lista para mostrar")
        win.show()
    })
})